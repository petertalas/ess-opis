importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var rawXArray = PVUtil.getDoubleArray(pvs[0]);
var rawYArray = PVUtil.getDoubleArray(pvs[1]);
var n = PVUtil.getDouble(pvs[2]);

var cutXArray = DataUtil.createDoubleArray(n);
var cutYArray = DataUtil.createDoubleArray(n);

/*ConsoleUtil.writeInfo(n);*/

for(var i=0; i<n; i++){
	cutXArray[i] = rawXArray[i];
	cutYArray[i] = rawYArray[i];
}

pvs[3].setValue(cutXArray);
pvs[4].setValue(cutYArray);