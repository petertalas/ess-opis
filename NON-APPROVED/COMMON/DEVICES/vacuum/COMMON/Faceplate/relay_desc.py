from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

macros = widget.getEffectiveMacros()
#'add', 'checkMacroName', 'class', 'empty', 'equals', 'expandValues', 'getClass', 'getNames', 'getValue', 'hashCode', 'isEmpty', 'merge', 'names', 'notify', 'notifyAll', 'toString', 'wait'
relay_desc = macros.getValue("RELAY{}_DESC".format(macros.getValue("RELAY")))
if relay_desc is not None:
    widget.setPropertyValue('tooltip', "{}: {}".format(PVUtil.getString(pvs[0]), relay_desc))
else:
    widget.setPropertyValue('tooltip', PVUtil.getString(pvs[0]))

