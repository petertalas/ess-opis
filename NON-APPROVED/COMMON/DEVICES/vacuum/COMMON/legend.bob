<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Vacuum System Legend</name>
  <macros>
    <ROOT>$(ESS_OPIS=/ess-opis)/NON-APPROVED</ROOT>
    <WIDGET_ROOT>$(ROOT)/COMMON/DEVICES/vacuum</WIDGET_ROOT>
    <vacSYMBOLS>$(ROOT)/COMMON/DEVICES/vacuum/symbols</vacSYMBOLS>
  </macros>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <text>            Vacuum System Legend</text>
    <width>800</width>
    <height>80</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="White" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="Primary Blue" red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VGC</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_gauge.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VGC - Cold Cathode Gauge</TOOLTIP>
      <vacDEV>vgc</vacDEV>
    </macros>
    <x>50</x>
    <y>110</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VGD</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_gauge.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VGD - Capacitance Gauge</TOOLTIP>
      <vacDEV>vgd</vacDEV>
    </macros>
    <x>150</x>
    <y>110</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VGP</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_gauge.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VGP - Pirani Gauge</TOOLTIP>
      <vacDEV>vgp</vacDEV>
    </macros>
    <x>250</x>
    <y>110</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VGR</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_gauge_vgr.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VGR - Partial Pressure Gauge</TOOLTIP>
      <vacDEV>vgr</vacDEV>
    </macros>
    <x>350</x>
    <y>110</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVMC</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_gauge_vvmc.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVMC - Mass Flow Meter</TOOLTIP>
      <vacDEV>vvmc</vacDEV>
    </macros>
    <x>450</x>
    <y>110</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VPI</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_pump_vpi.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VPI - Sputter Ion Pump</TOOLTIP>
      <vacDEV>vpi</vacDEV>
    </macros>
    <x>50</x>
    <y>210</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VPP</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_pump.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VPP - Primary Pump</TOOLTIP>
      <vacDEV>vpp</vacDEV>
    </macros>
    <x>150</x>
    <y>210</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VPDP</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_pump.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VPDP - Primary Pump</TOOLTIP>
      <vacDEV>vpdp</vacDEV>
    </macros>
    <x>250</x>
    <y>210</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VPT</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_pump.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-off.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VPT - Turbopump</TOOLTIP>
      <vacDEV>vpt</vacDEV>
    </macros>
    <x>350</x>
    <y>210</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVA</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVA - Angle Valve</TOOLTIP>
      <vacDEV>vva</vacDEV>
    </macros>
    <x>50</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVA straight</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <FLAVOR>-straight</FLAVOR>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVA - Straight Angle Valve</TOOLTIP>
      <vacDEV>vva</vacDEV>
    </macros>
    <x>150</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVF</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVF - Fast Valve</TOOLTIP>
      <vacDEV>vvf</vacDEV>
    </macros>
    <x>250</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVG</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVG - Gate Valve</TOOLTIP>
      <vacDEV>vvg</vacDEV>
    </macros>
    <x>350</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVM</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-not-controlled.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVM - Manual Valve</TOOLTIP>
      <vacDEV>vvm</vacDEV>
    </macros>
    <x>450</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
    <visible>false</visible>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>VVM</name>
    <file>$(vacSYMBOLS)/vvm/vvm-not-controlled.png</file>
    <x>450</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
    <tooltip>VVM - Manual Valve
(Not controlled)</tooltip>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVM Straight</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <FLAVOR>-straight</FLAVOR>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)$(FLAVOR)/$(vacDEV)-not-controlled.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVM - Straight Manual Valve</TOOLTIP>
      <vacDEV>vvm</vacDEV>
    </macros>
    <x>550</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
    <visible>false</visible>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>VVM Straight</name>
    <file>$(vacSYMBOLS)/vvm-straight/vvm-not-controlled.png</file>
    <x>550</x>
    <y>310</y>
    <width>50</width>
    <height>50</height>
    <tooltip>VVM - Straight Manual Valve
(Not controlled)</tooltip>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>VVS</name>
    <file>LEGEND/widget.bob</file>
    <macros>
      <LEGEND_DETAIL>legend_valve.bob</LEGEND_DETAIL>
      <SYMBOL>$(vacSYMBOLS)/$(vacDEV)/$(vacDEV)-closed.png</SYMBOL>
      <TITLE>$(TOOLTIP)</TITLE>
      <TOOLTIP>VVS - Sector Gate Valve</TOOLTIP>
      <vacDEV>vvs</vacDEV>
    </macros>
    <x>50</x>
    <y>410</y>
    <width>50</width>
    <height>50</height>
  </widget>
</display>
