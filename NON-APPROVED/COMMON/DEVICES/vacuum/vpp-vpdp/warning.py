#Generated from VACUUM_VAC-VPP_VAC-VPDP.def at 2020-06-11_16:17:53
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

msg  = ""
code = 0

if PVUtil.getLong(pvs[0]):
    code = PVUtil.getLong(pvs[1])

    msgs = dict({
                 99 : "99 - Pump Not Started",
                 98 : "98 - Pump Starting Prevented by Tripped Interlock",
                 5 : "5 - Pressure Interlock Bypassed",
                 4 : "4 - Hardware Interlock Bypassed",
                 3 : "3 - Software Interlock Bypassed",
                 0 : ""
                })

    try:
        msg = msgs[code]
    except KeyError:
        msg = "Warning Code: " + PVUtil.getString(pvs[1])
        ScriptUtil.getLogger().severe("Unknown warning code {} : {}".format(pvs[1], code))

try:
    pvs[2].setValue(msg)
except:
    if widget.getType() != "action_button":
        widget.setPropertyValue("text", msg)
    widget.setPropertyValue("tooltip", msg)
